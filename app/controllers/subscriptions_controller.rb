class SubscriptionsController < ApplicationController
  before_action :set_subscription, only: [:show, :update, :destroy]
  before_action :authenticate_user!
  before_action :open_subscription, only: :create
  before_action :can_post_notes, only: :update
  load_and_authorize_resource
  # GET /subscriptions
  def index

    if current_user.student
      @subscriptions = Subscription.where(student_id: current_user.id)
    else
      @subscriptions = Subscription.all
    end
    render json: @subscriptions
  end

  # GET /subscriptions/1
  def show
    render json: @subscription
  end

  # POST /subscriptions
  def create
    
    @subscription = Subscription.new(subscription_params)
    if @subscription.student_id != current_user.id
      @subscription.errors.add("You cant create subscriptions for this id")
    else  

      if @subscription.save
        render json: @subscription, status: :created, location: @subscription
      else
        render json: @subscription.errors, status: :unprocessable_entity
      end
    end

  end

  # PATCH/PUT /subscriptions/1
  def update
    if @subscription.update(subscription_params)
      render json: @subscription
    else
      render json: @subscription.errors, status: :unprocessable_entity
    end
  end

  # DELETE /subscriptions/1
  def destroy
    @subscription.destroy
  end

  private
    def open_subscription
      if Classroom.find_by!(id: :classroom_id).pendant
        return true
      end
    end
    def can_post_notes
      if current_user.teacher
        puts "entrou aqui"
        if Teacher.find_by(user_id: current_user.id).id == Classroom.find_by(id: @subscription.classroom_id).teacher_id
          puts "---------------------"
          puts @subscription.status
          puts "---------------------"

          if @subscription.status == "Validated"
            return true
          else  
            @subscription.errors.add("this classroom isnt validated yet")
          end

        else  
          @subscription.errors.add("you arent the teacher for this classroom")
        end
      else  
        @subscription.errors.add("you arent a teacher")
      end
      
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_subscription
      @subscription = Subscription.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def subscription_params
      params.require(:subscription).permit(:note_final, :note_p1, :note_p2, :status, :student_id, :classroom_id)
    end
end
