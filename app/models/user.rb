class User < ApplicationRecord
  has_one :teacher
  has_one :student
  validates :password, length: { minimum: 6 } 
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }
  validates :email, :format => { :with => /id.uff.br\Z/i, :on => :create }
  # true

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  enum kind: [
    :student,
    :admin,
    :teacher,
    :secretary
  ]

  devise  :database_authenticatable,
          :jwt_authenticatable,
          
          jwt_revocation_strategy: JwtBlacklist
end
