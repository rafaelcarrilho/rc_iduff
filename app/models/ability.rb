# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    alias_action :create, :read, :update, :destroy, to: :crud
    
    if user.admin?
      can :manage, :all
    end
    if user.secretary?
      can :manage, :all #[Subject, License, Requirement, Classroom, Subscription]
    end
    if user.student?
      can :read, Classroom
      can :crud, Subscription
    end
    if user.teacher?
      
      can :update, Subscription
      
    end
  end
end

