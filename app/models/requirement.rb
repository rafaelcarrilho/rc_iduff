class Requirement < ApplicationRecord
  belongs_to :subject
  belongs_to :discipline, class_name: "Subject"

  validate :cant_be_discipline_to_itself, :cant_have_circular_relation
  def cant_be_discipline_to_itself
    if subject_id == discipline_id
      errors.add(:discipline_id, "This subject cant be prerequisite to itself")
    end
  end
  def cant_have_circular_relation
    tamanhoTabela = Requirement.count
    objectList = Requirement.where(subject_id: discipline_id).take(tamanhoTabela)

    if objectList.length == 0
      return true
    end
    objectList.each do |i|
      if i.discipline_id == subject_id
        errors.add(:subject_id, "Cant have circular relation")
      end  
    end     
  end

end
