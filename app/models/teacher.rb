class Teacher < ApplicationRecord
  belongs_to :user
  has_many :classrooms
  has_many :licenses
  has_many :subjects, through: :licenses

end
