class Student < ApplicationRecord
    belongs_to :user
    has_many :subscriptions
    has_many :classrooms, through: :subscription 
end
