class Classroom < ApplicationRecord
  belongs_to :teacher, optional: true

  enum status: [
    "pendant",
    "active"
  ]

  validate :when_try_to_active_the_classroom
  def when_try_to_active_the_classroom
    if status == "active"
      if not License.find_by(teacher_id: teacher_id, subject_id: subject_id)
        errors.add(:teacher_id, "This teacher isnt licensed to this subject")
      end
      inscripted = Subscription.where(classroom_id: id)
      puts "-----------------"
      puts inscripted
      puts "-----------------"
      inscripted.each do |i|
        i.update(status:"Validated")
      
      end
    end
    
  end
end

