class Subscription < ApplicationRecord
  belongs_to :student
  belongs_to :classroom
  enum status: [
    "Registered",
    "Validated"
  ]
  

  
end
