require 'test_helper'

class SubscriptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subscription = subscriptions(:one)
  end

  test "should get index" do
    get subscriptions_url, as: :json
    assert_response :success
  end

  test "should create subscription" do
    assert_difference('Subscription.count') do
      post subscriptions_url, params: { subscription: { classroom_id: @subscription.classroom_id, note_final: @subscription.note_final, note_p1: @subscription.note_p1, note_p2: @subscription.note_p2, status: @subscription.status, students_id: @subscription.students_id } }, as: :json
    end

    assert_response 201
  end

  test "should show subscription" do
    get subscription_url(@subscription), as: :json
    assert_response :success
  end

  test "should update subscription" do
    patch subscription_url(@subscription), params: { subscription: { classroom_id: @subscription.classroom_id, note_final: @subscription.note_final, note_p1: @subscription.note_p1, note_p2: @subscription.note_p2, status: @subscription.status, students_id: @subscription.students_id } }, as: :json
    assert_response 200
  end

  test "should destroy subscription" do
    assert_difference('Subscription.count', -1) do
      delete subscription_url(@subscription), as: :json
    end

    assert_response 204
  end
end
