class RemoveDisciplineFromRequirements < ActiveRecord::Migration[5.2]
  def change
    remove_column :requirements, :discipline, :integer
  end
end
