class AddStudentToSubscription < ActiveRecord::Migration[5.2]
  def change
    add_reference :subscriptions, :student, foreign_key: true
  end
end
