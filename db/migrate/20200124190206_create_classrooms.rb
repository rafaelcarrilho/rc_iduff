class CreateClassrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :classrooms do |t|
      t.string :name
      t.boolean :monday
      t.boolean :tuesday
      t.boolean :wednesday
      t.boolean :thursday
      t.boolean :friday
      t.time :initial_time
      t.time :final_time
      t.integer :status
      t.references :teacher, foreign_key: true

      t.timestamps
    end
  end
end
