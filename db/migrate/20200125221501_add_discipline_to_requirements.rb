class AddDisciplineToRequirements < ActiveRecord::Migration[5.2]
  def change
    add_column :requirements, :discipline_id, :integer
  end
end
