class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :subscriptions do |t|
      t.float :note_final
      t.float :note_p1
      t.float :note_p2
      t.integer :status
      t.references :students, foreign_key: true
      t.references :classroom, foreign_key: true

      t.timestamps
    end
  end
end
