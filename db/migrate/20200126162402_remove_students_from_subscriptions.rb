class RemoveStudentsFromSubscriptions < ActiveRecord::Migration[5.2]
  def change
    remove_reference :subscriptions, :students, foreign_key: true
  end
end
